use std::fs::File;
use std::io::prelude::*;

enum Command {
    Noop,
    Addx { val: i32 }
}

use Command::*;

fn main() -> std::io::Result<()> {
    let mut file = File::open("input")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let lines: Vec<Command> = contents.trim()
        .split("\n")
        .map(|s| {
            let mut s = s.trim().split(" ");
            if s.next().unwrap().trim() == "noop" {
                Noop
            }
            else {
                Addx { val: s.next().unwrap().trim().parse().unwrap() }
            }
        })
        .collect();

    let mut x: Vec<i32> = vec![1, 1];

    for v in lines.iter() {
        let i = x.len() - 1;
        let previous = x[i];
        x.push(previous);
        match v {
            Addx { val } => {
                x.push(previous + val);
            },
            Noop => {}
        }
    }

    let signal: Vec<i32> = x.iter()
        .enumerate()
        .map(|(i, v)| {
            *v * (i as i32)
        })
        .collect();

    let p1 = signal[20] + signal[60] + signal[100] + signal[140] + signal[180] + signal[220];

    for i in 0..6 {
        for j in 0..40 {
            let k = j + 1 + i * 40;
            let j = j as i32;
            if x[k] == j || x[k] - 1 == j || x[k] + 1 == j {
                print!("#");
            }
            else {
                print!(" ");
            }
        }
        print!("\n");
    }

    println!("\n\n");

    let p2 = 0;

    println!("{}, {}", p1, p2);
    Ok(())
}

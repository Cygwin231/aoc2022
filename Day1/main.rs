use std::fs::File;
use std::io::prelude::*;

fn main() -> std::io::Result<()> {
    let mut file = File::open("input")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let mut lines: Vec<i32> = contents
        .trim()
        .split("\n\n")
        .map(|s|
             s.trim()
             .split("\n")
             .map(|c|
                  c.trim()
                  .parse::<i32>()
                  .unwrap())
             .sum()
        )
        .collect();
    lines.sort_unstable();
    lines.reverse();
    
    let top = lines[0] + lines[1] + lines[2];

    println!("First: {}, Three: {}", lines[0], top);
    Ok(())
}

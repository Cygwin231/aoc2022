use std::fmt;
use std::fs::File;
use std::io::prelude::*;

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
enum Space {
    Sand,
    Rock,
    Air
}

impl fmt::Display for Space {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let c;
        match self {
            Sand => c = "O",
            Rock => c = "#",
            Air => c = " "
        }
        write!(f, "{}", c)
    }
}

use Space::*;

fn main() -> std::io::Result<()> {
    let mut file = File::open("input")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;

    let mut map: Vec<Vec<Space>> = Vec::new();
    let mut base: Vec<Space> = vec![Air];

    for i in contents.trim().split("\n") {
        let mut previous: Option<(usize, usize)> = None;
        for s in i.trim().split(" -> ") {
            let coord: Vec<usize> = s.trim()
                .split(",")
                .map(|s| {
                    s.parse().unwrap()
                })
                .collect();
            println!("{:?}", coord);

            if map.len() <= coord[1] {
                map.resize(coord[1] + 1, base.clone());
            }
            if map[0].len() <= coord[0] {
                base.resize(coord[0] + 1, Air);
                for q in map.iter_mut() {
                    q.resize(coord[0] + 1, Air);
                }
            }

            assert_eq!(coord.len(), 2);

            if let Some((x, y)) = previous {
                if x == coord[0] {
                    let first;
                    let second;

                    if y > coord[1] {
                        first = coord[1];
                        second = y;
                    }
                    else {
                        first = y;
                        second = coord[1];
                    }

                    for k in  first..=second {
                        map[k][x] = Rock;
                    }
                }
                else if y == coord[1] {
                    let first;
                    let second;

                    if x > coord[0] {
                        first = coord[0];
                        second = x;
                    }
                    else {
                        first = x;
                        second = coord[0];
                    }

                    for j in  first..=second {
                        map[y][j] = Rock;
                    }

                }
                else {
                    panic!()
                }
            }

            previous = Some((coord[0], coord[1]));
        }
    }

    for i in &map {
        for j in i {
            print!("{}", j);
        }
        println!("");
    }


    println!("{} tall, {} wide", map.len(), map[0].len());
    
    map.push(base.clone());
    map.push(vec![Rock; map[0].len()]);

    let input = (500, 0);
    if map[0].len() <= 500 {
        panic!("0, 0")
    }
    
    let mut p1: usize = 0;

    'outer: loop {
        let mut current = input;
        'grain: loop {
            if map[current.1].len() <= current.0 + 1 {
                base.resize(current.0 + 2, Air);
                let len = map.len() - 1;
                map[len].resize(current.0 + 2, Rock);
                for q in map.iter_mut() {
                    q.resize(current.0 + 2, Air);
                }
            }
            if map[input.1][input.0] == Sand {
                break 'outer;
            }
            else if map[current.1 + 1][current.0] == Air {
                current = (current.0, current.1 + 1);
            }
            else if map[current.1 + 1][current.0 - 1] == Air {
                current = (current.0 - 1, current.1 + 1);
            }
            else if map[current.1 + 1][current.0 + 1] == Air {
                current = (current.0 + 1, current.1 + 1);
            }
            else {
                map[current.1][current.0] = Sand;
                p1 += 1;
                break 'grain;
            }
        }
    }

    for i in &map {
        for j in i {
            print!("{}", j);
        }
        println!("");
    }

    let p2 = 0;

    println!("{}, {}", p1, p2);
    Ok(())
}

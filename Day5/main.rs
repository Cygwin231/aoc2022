use std::ops::Range;
use std::fs::File;
use std::io::prelude::*;

#[derive(Clone, PartialEq, Eq, Debug)]
struct Crane {
    crates: Vec<String>
}

impl Crane {
    fn transfer(&mut self, num_crates: usize, start: usize, end: usize) {
        let moving: String = self.crates[start - 1][self.crates[start - 1].len() - num_crates..].to_string();
        self.crates[end - 1].push_str(&moving);
        for _ in 0..num_crates {
            self.crates[start - 1].pop();
        }
    }
}

fn main() -> std::io::Result<()> {
    let mut file = File::open("input")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let mut lines: Vec<Vec<String>> = contents
        .trim()
        .split("\n\n")
        .map(|s| {
            s.split("\n")
            .map(|s| {
                s.to_string()
            })
            .collect()
        })
        .collect();

    let mut dock: Crane = Crane { crates: Vec::new() };
    lines[0].reverse();
    let index: &str = &lines[0][0];
    for (i, v) in index.chars().enumerate() {
        println!("{}, {}", i, v);
        if v.is_numeric() {
            dock.crates.push(lines[0][1][i..i+1].to_string());
            for c in lines[0].iter().skip(2) {
                dock.crates[usize::from_str_radix(&v.to_string(), 10).unwrap() - 1].push_str(&c[i..i+1]);
            }
        }
    }

    dock.crates = dock.crates.iter()
        .map(|s| {
            s.trim()
            .to_string()
        })
        .collect();

    println!("{:?}", dock);

    for i in lines[1].iter() {
        let command: Vec<&str> = i.split(" ").collect();
        dock.transfer(usize::from_str_radix(command[1], 10).unwrap(), usize::from_str_radix(command[3], 10).unwrap(), usize::from_str_radix(command[5], 10).unwrap())
    }
    
    let p1: String = dock.crates.iter()
        .map(|s| {
            s.chars().last().unwrap()
        })
        .collect();
    let p2: usize = 0;

    println!("{}, {}", p1, p2);
    Ok(())
}

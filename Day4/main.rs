use std::ops::Range;
use std::fs::File;
use std::io::prelude::*;

fn main() -> std::io::Result<()> {
    let mut file = File::open("input")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let lines: Vec<String> = contents
        .trim()
        .split("\n")
        .map(|s| {
            s.trim().to_string()
        }
        )
        .collect();
    
    let p1: usize = lines.iter()
        .filter(|s| {
            let s: Vec<Range<i32>> = s.split(",")
                .map(|s| {
                    let s: Vec<i32> = s.trim()
                        .split("-")
                        .map(|s| s.trim().parse().unwrap())
                        .collect();
                    s[0]..(s[1] + 1)
                })
                .collect();
            
            let mut s1: String = " ".to_string();
            let mut s2: String = " ".to_string();
            for i in s[0].clone() {
                s1 = s1 + &(i.to_string()) + " ";
            }
            for i in s[1].clone() {
                s2 = s2 + &(i.to_string()) + " ";
            }
            s1.find(&s2) != None || s2.find(&s1) != None
        })
        .count();

    let p2: usize = lines.iter()
        .filter(|s| {
            let s: Vec<Range<i32>> = s.split(",")
                .map(|s| {
                    let s: Vec<i32> = s.trim()
                        .split("-")
                        .map(|s| s.trim().parse().unwrap())
                        .collect();
                    s[0]..(s[1] + 1)
                })
                .collect();
            
            for i in s[0].clone() {
                if s[1].contains(&i) {
                    return true
                }
            }
            for i in s[1].clone() {
                if s[0].contains(&i) {
                    return true
                }
            }
            false
        })
        .count();
    

    println!("{}, {}", p1, p2);
    Ok(())
}

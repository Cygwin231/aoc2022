use std::fs::File;
use std::io::prelude::*;

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
enum Operation {
    Addition(Option<usize>),
    Multiply(Option<usize>)
}

use Operation::*;

#[derive(Clone, PartialEq, Eq, Debug)]
struct Monkey {
    items: Vec<usize>,
    operation: Operation,
    test: usize,
    test_true: usize,
    test_false: usize,
    inspections: usize
}

fn main() -> std::io::Result<()> {
    let mut file = File::open("input")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let mut monkeys: Vec<Monkey> = contents.trim()
        .split("\n\n")
        .map(|s| {
            let s: Vec<String> = s.trim()
                .split("\n")
                .map(|s| {
                    s.trim().to_string()
                })
                .collect();

            let items: Vec<usize> = s[1][16..].split(", ")
                .map(|s| {
                    s.trim().parse().unwrap()
                })
                .collect();

            let op = s[2][17..].split(" ")
                .map(|s| {
                    s.trim()
                })
                .collect::<Vec<&str>>();
            let val: Option<usize> = match op[2] {
                "old" => None,
                v => Some(v.parse().unwrap())
            };
            let operation: Operation = match op[1] {
                "+" => { Addition(val) },
                "*" => { Multiply(val) },
                _ => panic!()
            };

            let test: usize = s[3][18..].trim().parse().unwrap();

            let test_true: usize = s[4].chars().last().unwrap().to_string().parse().unwrap();

            let test_false: usize = s[5].chars().last().unwrap().to_string().parse().unwrap();

            Monkey { items, operation, test, test_true, test_false, inspections: 0 }
        })
        .collect();

    println!("{:#?}", monkeys);

    let mut trunc: usize = 1;

    for i in &monkeys {
        trunc = trunc * i.test;
    }

    println!("{}", trunc);

    for _ in 0..10000 {
        for i in 0..monkeys.len() {
            let mut items = monkeys[i].items.clone();
            for (_j, v) in items.iter_mut().enumerate() {
                match monkeys[i].operation {
                    Addition(val) => {
                        match val {
                            Some(val) => *v = *v + val,
                            None => *v = *v + *v
                        }
                    },
                    Multiply(val) => {
                        match val {
                            Some(val) => *v = *v * val,
                            None => *v = *v * *v
                        }
                    }
                }

                *v = *v % trunc;

                if *v % monkeys[i].test == 0 {
                    let test_true = monkeys[i].test_true;
                    monkeys[test_true].items.push(*v);
                }
                else {
                    let test_false = monkeys[i].test_false;
                    monkeys[test_false].items.push(*v);
                }

                let inspections = monkeys[i].inspections;
                monkeys[i].inspections = inspections + 1;
            }
            monkeys[i].items.clear();
        }
    }
    
    let mut inspections: Vec<usize> = monkeys.iter()
        .map(|s| {
            s.inspections
        })
        .collect();
    inspections.sort();
    inspections.reverse();

    let p1 = inspections[0] * inspections[1];
    let p2 = 0;

    println!("{}, {}", p1, p2);
    Ok(())
}

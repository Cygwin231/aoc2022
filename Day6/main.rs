use std::fs::File;
use std::io::prelude::*;

fn check(content: &str) -> bool {
    let mut check: bool = true;
    for (i, v) in content.chars().enumerate() {
        if content.find(&v.to_string()) != Some(i) {
            check = false;
        }
    }
    check
}

fn main() -> std::io::Result<()> {
    let mut file = File::open("input")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let lines: Vec<char> = contents.trim().chars().collect();

    let offset: usize = 4;
    let mut p1: usize = 0;
    let mut marker = "".to_string();
    for j in 0..offset {
        let i = offset - 1 - j;
        marker.push(lines[i]);
    }
    println!("{}", marker);
    if check(&marker) {
        p1 = offset;
    }
    else {
        for (i, v) in lines.iter().skip(offset).enumerate() {
            marker.pop();
            marker.insert(0, *v);
            if check(&marker) {
                p1 = i + offset + 1;
                break;
            }
        }
    }


    let offset: usize = 14;
    let mut p2: usize = 0;
    let mut marker = "".to_string();
    for j in 0..offset {
        let i = offset - 1 - j;
        marker.push(lines[i]);
    }
    println!("{}", marker);
    if check(&marker) {
        p2 = offset;
    }
    else {
        for (i, v) in lines.iter().skip(offset).enumerate() {
            marker.pop();
            marker.insert(0, *v);
            if check(&marker) {
                p2 = i + offset + 1;
                break;
            }
        }
    }
    println!("{}, {}", p1, p2);
    Ok(())
}

use std::fs::File;
use std::io::prelude::*;

#[derive(PartialEq, Eq)]
enum Hand {
    Rock,
    Paper,
    Scissors
}

use Hand::*;

impl Hand {
    fn next(&self) -> Hand {
        match self {
            Rock => Paper,
            Paper => Scissors,
            Scissors => Rock
        }
    }

    fn previous(&self) -> Hand {
        match self {
            Rock => Scissors,
            Paper => Rock,
            Scissors => Paper
        }
    }

    fn parse(input: &str) -> Hand {
        match input {
            "A" | "X" => Hand::Rock,
            "B" | "Y" => Hand::Paper,
            "C" | "Z" => Hand::Scissors,
            _ => panic!("Not valid action!")
        }
    }

    fn score(&self) -> usize {
        match self {
            Rock => 1,
            Paper => 2,
            Scissors => 3
        }
    }

    fn battle_against(&self, hand: &Hand) -> usize {
        if *self == *hand { 3 }
        else if *self == hand.next() { 6 }
        else if *self == hand.previous() { 0 }
        else { panic!() }
    }
}

fn main() -> std::io::Result<()> {
    let mut file = File::open("input")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let lines: Vec<(usize, usize)> = contents
        .trim()
        .split("\n")
        .map(|s| {
            let s: Vec<Hand> = s.trim()
            .split(" ")
            .map(|s|
                 Hand::parse(s.trim())
            )
            .collect();

            let p1 = s[1].battle_against(&s[0]) + s[1].score();
            
            let p2;
            if s[1] == Rock {
                p2 = s[0].previous().score();
            }
            else if s[1] == Paper {
                p2 = 3 + s[0].score();
            }
            else if s[1] == Scissors {
                p2 = 6 + s[0].next().score();
            }
            else {
                panic!()
            }
            
            (p1, p2)
        }
        )
        .collect();
    
    let p1: usize = lines.iter().map(|(p1, _)| p1).sum();
    let p2: usize = lines.iter().map(|(_, p2)| p2).sum();

    println!("{}, {}", p1, p2);
    Ok(())
}

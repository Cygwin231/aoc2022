use std::fs::File;
use std::io::prelude::*;

fn main() -> std::io::Result<()> {
    let mut file = File::open("input")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let lines: Vec<(i32, i32)> = contents
        .trim()
        .split("\n")
        .map(|s| {
            let s: Vec<i32> = s.trim()
            .split(" ")
            .map(|s|
                 match s.trim() {
                    "A" | "X" => 0,
                    "B" | "Y" => 1,
                    "C" | "Z" => 2,
                    _ => panic!()
                 }
            )
            .collect();

            let p1 = (s[1] + 1) + match (s[0] - s[1] + 3) % 3 {
                0 => 3,
                1 => 0,
                2 => 6,
                _ => panic!()
            };
            
            let p2;
            if s[1] == 0 {
                p2 = ((s[0] + 2) % 3) + 1;
            }
            else if s[1] == 1 {
                p2 = 3 + s[0] + 1;
            }
            else if s[1] == 2 {
                p2 = 6 + ((s[0] + 1) % 3) + 1;
            }
            else {
                panic!()
            }
            
            (p1, p2)
        }
        )
        .collect();
    
    let p1: i32 = lines.iter().map(|(p1, _)| p1).sum();
    let p2: i32 = lines.iter().map(|(_, p2)| p2).sum();

    println!("{}, {}", p1, p2);
    Ok(())
}

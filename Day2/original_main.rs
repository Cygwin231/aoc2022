use std::fs::File;
use std::io::prelude::*;

fn main() -> std::io::Result<()> {
    let mut file = File::open("input")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let mut lines: Vec<i32> = contents
        .trim()
        .split("\n")
        .map(|s| {
             let s: Vec<&str> = s.trim()
             .split(" ")
             .map(|s|
                  s.trim()
             )
             .collect();
             
             let mut score = 0;
             if s[1] == "Y" {
                 score += 3
             }
             if s[1] == "Z" {
                 score += 6
             }
             if (s[1] == "X" && s[0] == "B") || (s[1] == "Z" && s[0] == "C") || (s[1] == "Y" && s[0] == "A") {
                 score += 1
             }
             if (s[1] == "X" && s[0] == "C") || (s[1] == "Z" && s[0] == "A") || (s[1] == "Y" && s[0] == "B") {
                 score += 2
             }
             if (s[1] == "X" && s[0] == "A") || (s[1] == "Z" && s[0] == "B") || (s[1] == "Y" && s[0] == "C") {
                 score += 3
             }
             score
        }
        )
        .collect();
    
    let total: i32 = lines.iter().sum();

    println!("{:?}", total);
    Ok(())
}

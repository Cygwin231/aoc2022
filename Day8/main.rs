use std::fs::File;
use std::io::prelude::*;

fn main() -> std::io::Result<()> {
    let mut file = File::open("input")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let lines: Vec<Vec<i32>> = contents.trim()
        .split("\n")
        .map(|s| {
            s.trim()
            .chars()
            .map(|s| {
                i32::from_str_radix(&s.to_string(), 10).unwrap()
            })
            .collect()
        })
        .collect();

    let mut vis: Vec<Vec<bool>> = lines.iter()
        .map(|s| {
            s.iter()
            .map(|_| {
                false
            })
            .collect()
        })
        .collect();

    let mut tallest;

    for i in 0..vis.len() {
        tallest = -1;
        for j in 0..vis[i].len() {
            if lines[i][j] > tallest {
                tallest = lines[i][j];
                vis[i][j] = true;
            }
        }
    }

    for i in 0..vis.len() {
        tallest = -1;
        for j in 0..vis[i].len() {
            let j = vis[i].len() - j - 1;
            if lines[i][j] > tallest {
                tallest = lines[i][j];
                vis[i][j] = true;
            }
        }
    }

    for j in 0..vis[0].len() {
        tallest = -1;
        for i in 0..vis.len() {
            if lines[i][j] > tallest {
                tallest = lines[i][j];
                vis[i][j] = true;
            }
        }
    }

    for j in 0..vis[0].len() {
        tallest = -1;
        for i in 0..vis.len() {
            let i = vis.len() - i - 1;
            if lines[i][j] > tallest {
                tallest = lines[i][j];
                vis[i][j] = true;
            }
        }
    }

    let human: Vec<Vec<i32>> = vis.iter()
        .map(|s| {
            s.iter()
            .map(|s| {
                if *s {
                    1
                }
                else {
                    0
                }
            })
            .collect()
        })
        .collect();

    for i in human {
        for j in i {
            print!("{}", j);
        }
        print!("\n");
    }

    print!("\n");

    let p1: usize = vis.iter()
        .map(|s| {
            s.iter()
            .filter(|k| {
                **k
            })
            .count()
        })
        .sum();

    let mut score: Vec<Vec<i32>> = lines.iter()
        .map(|s| {
            s.iter()
            .map(|_| {
                0
            })
            .collect()
        })
        .collect();

    for (i, w) in lines.iter().enumerate() {
        for (j, v) in w.iter().enumerate() {
            if i > 0 && i < lines.len() - 1 && j > 0 && j < lines[i].len() - 1 {
                let mut point = 1;
                let mut mult = 0;
                print!("\n{}, {}, {}", i, j, point);
                let mut wat: Vec<&Vec<i32>> = lines[..i].iter().collect();
                wat.reverse();
                println!("{:?}", wat);
                for tree in wat {
                    println!("{:?}", tree);
                    mult = mult + 1;
                    if &tree[j] >= v {
                        break;
                    }
                }
                point = point * mult;
                print!(", {}", mult);

                mult = 0;
                for tree in &lines[i+1..] {
                    mult = mult + 1;
                    if &tree[j] >= v {
                        break;
                    }
                }
                point = point * mult;
                print!(", {}", mult);
                
                mult = 0;
                let mut wat: Vec<&i32> = lines[i][..j].iter().collect();
                wat.reverse();
                for tree in wat {
                    mult = mult + 1;
                    if tree >= v {
                        break;
                    }
                }
                point = point * mult;
                print!(", {}", mult);
                
                mult = 0;
                for tree in &lines[i][j+1..] {
                    mult = mult + 1;
                    if tree >= v {
                        break;
                    }
                }
                point = point * mult;
                print!(", {}\n", mult);
                print!("{}\n\n", point);
                score[i][j] = point;
            }
        }
    }

    let p2: i32 = *score.iter()
        .map(|s| {
            s.iter()
            .max()
            .unwrap()
        })
        .max()
        .unwrap();

    println!("{}, {}", p1, p2);
    Ok(())
}

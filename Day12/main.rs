use std::fs::File;
use std::io::prelude::*;

fn main() -> std::io::Result<()> {
    let mut file = File::open("input")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let mut map: Vec<Vec<char>> = contents.trim()
        .split("\n")
        .map(|s| {
            s.trim()
            .chars()
            .collect()
        })
        .collect();

    let mut start_x: usize = 0;
    let mut start_y: usize = 0;
    
    let mut end_x: usize = 0;
    let mut end_y: usize = 0;

    for (i, v) in map.iter().enumerate() {
        for (j, s) in v.iter().enumerate() {
            if *s == 'S' {
                start_x = i;
                start_y = j;
            }
            else if *s == 'E' {
                end_x = i;
                end_y = j;
            }
        }
    }

    map[start_x][start_y] = 'a';
    map[end_x][end_y] = 'z';

    let mut starts: Vec<(usize, usize)> = Vec::new();

    for (i, v) in map.iter().enumerate() {
        for (j, s) in v.iter().enumerate() {
            if *s == 'a' {
                starts.push((i, j));
            }
        }
    }

    let mut found: Vec<usize> = Vec::new();

    let mut solutions: Vec<Vec<(usize, usize)>> = Vec::new();

    for zed in starts {
        solutions.push(vec![zed]);
    }

    'outer: loop {
        //println!("Zed - {}", solutions.len());
        if solutions.len() == 0 {
            break 'outer;
        }
        for (_, v) in solutions.clone().iter().enumerate() {
            let s = v.iter().last().unwrap();
            if s.0 == end_x && s.1 == end_y {
                found.push(v.len() - 1);
                break 'outer;
            }
            if found.len() > 0 {
                found.sort();
                if v.len() > found[0] {
                    break 'outer;
                }
            }
            if solutions.len() == 0 {
                break 'outer;
            }
            let mut possible: Vec<(usize, usize)> = Vec::new();
            if s.0 > 0 {
                possible.push((s.0 - 1, s.1));
            }
            if s.0 < map.len() - 1 {
                possible.push((s.0 + 1, s.1));
            }
            if s.1 > 0 {
                possible.push((s.0, s.1 - 1));
            }
            if s.1 < map[0].len() - 1 {
                possible.push((s.0, s.1 + 1));
            }

            possible = possible.iter()
                .map(|p| *p)
                .filter(|(i, j)| {
                    //println!("{}, {}, {}, {}", i, j, s.0, s.1);
                    let mut contains = false;
                    for s in solutions.iter() {
                        if s.contains(&(*i, *j)) {
                            contains = true;
                        }
                    }
                    if map[*i][*j].to_digit(36).unwrap() - 1 > map[s.0][s.1].to_digit(36).unwrap() {
                        false
                    }
                    else if v.contains(&(*i, *j)){
                        false
                    }
                    else if contains {
                        false
                    }
                    else {
                        true
                    }
                })
                .collect();

            for p in possible {
                let mut sol = v.clone();
                sol.push(p);
                solutions.push(sol);
            }
            solutions.sort();
            solutions.remove(solutions.binary_search(v).unwrap());
        }
    }

    let p1 = 0;
    found.sort();
    let p2 = found[0];

    println!("{}, {}", p1, p2);
    Ok(())
}

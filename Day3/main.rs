use std::fs::File;
use std::io::prelude::*;

fn main() -> std::io::Result<()> {
    let mut file = File::open("input")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let lines: Vec<String> = contents
        .trim()
        .split("\n")
        .map(|s| {
            s.trim().to_string()
        }
        )
        .collect();
    
    let p1: i32 = lines.iter()
        .map(|s| {
            let mid = s.len() / 2;
            let c1 = &s[..mid];
            let c2 = &s[mid..];

            let mut value: char = 'a'; 
            for c in c1.chars() {
                if c2.find(c) != None {
                    value = c;
                    break;
                }
            }
            if value.is_uppercase() {
                i32::from_str_radix(&value.to_string(), 36).unwrap() - 9 + 26
            }
            else {
                i32::from_str_radix(&value.to_string(), 36).unwrap() - 9
            }
        })
        .sum();

    let mut p2: i32 = 0;
    for i in 0..(lines.len() / 3) {
        for j in lines[3 * i].chars() {
            if lines[(3 * i) + 1].find(j) != None && lines[(3 * i) + 2].find(j) != None {
                p2 = p2 + if j.is_uppercase() {
                    i32::from_str_radix(&j.to_string(), 36).unwrap() - 9 + 26
                }
                else {
                    i32::from_str_radix(&j.to_string(), 36).unwrap() - 9
                };
                break;
            }
        }
    }

    println!("{}, {}", p1, p2);
    Ok(())
}

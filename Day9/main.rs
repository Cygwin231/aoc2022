use std::collections::HashMap;
use std::fs::File;
use std::io::prelude::*;

fn update_tail(head_dif_x: i32, head_dif_y: i32) -> (i32, i32) {
    let mut tail_dif_x = 0;
    let mut tail_dif_y = 0;

    if head_dif_x.abs() > 1 || head_dif_y.abs() > 1 {
        tail_dif_x = 1 * head_dif_x.signum();
        tail_dif_y = 1 * head_dif_y.signum();
    }

    (tail_dif_x, tail_dif_y)
}

fn main() -> std::io::Result<()> {
    let mut file = File::open("input")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let lines: Vec<(char, i32)> = contents.trim()
        .split("\n")
        .map(|s| {
            let s = s.trim();
            (s[..1].parse().unwrap(), s[2..].parse().unwrap())
        })
        .collect();

    let mut knot_xy: Vec<(i32, i32)> = vec![(0, 0), (0, 0), (0, 0), (0, 0), (0, 0), (0, 0), (0, 0), (0, 0), (0, 0), (0, 0)];

    let mut touched1: HashMap<(i32, i32), bool> = HashMap::new();
    let mut touched2: HashMap<(i32, i32), bool> = HashMap::new();
    touched1.insert(knot_xy[1], true);
    touched2.insert(knot_xy[9], true);

    for (dir, val) in lines {
        for _ in 0..val {
            match dir {
                'R' => {
                    knot_xy[0].0 = knot_xy[0].0 + 1;
                },
                'L' => {
                    knot_xy[0].0 = knot_xy[0].0 - 1;
                },
                'U' => {
                    knot_xy[0].1 = knot_xy[0].1 + 1;
                },
                'D' => {
                    knot_xy[0].1 = knot_xy[0].1 - 1;
                },
                _ => {
                    panic!()
                }
            }
            for i in 1..knot_xy.len() {
                let (dif_x, dif_y) = update_tail(knot_xy[i-1].0 - knot_xy[i].0, knot_xy[i-1].1 - knot_xy[i].1);
                knot_xy[i].0 = knot_xy[i].0 + dif_x;
                knot_xy[i].1 = knot_xy[i].1 + dif_y;
            }

            println!("{:#?}", knot_xy);

            touched1.insert(knot_xy[1], true);
            touched2.insert(knot_xy[9], true);
        }
        println!("\n\n");
    }

    let p1 = touched1.iter().count();
    let p2 = touched2.iter().count();

    println!("{}, {}", p1, p2);
    Ok(())
}

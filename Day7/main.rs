use std::rc::Rc;
use std::cell::RefCell;
use std::fs::File;
use std::io::prelude::*;

#[derive(PartialEq, Eq, Debug)]
enum Data {
    Folder { name: String, size: usize, data: Vec<Rc<RefCell<Data>>>, container: Option<Rc<RefCell<Data>>> },
    File { name: String, size: usize, container: Option<Rc<RefCell<Data>>> }
}

use Data::*;

fn main() -> std::io::Result<()> {
    let mut file = File::open("input")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let lines: Vec<Vec<String>> = contents.trim()
        .split("\n")
        .map(|s| {
            s.trim()
            .split(" ")
            .map(|s| {
                s.trim()
                .to_string()
            })
            .collect()
        })
        .collect();

    let mut folders: Vec<Rc<RefCell<Data>>> = Vec::new();
    let storage: Data = Folder { name: "/".to_string(), size: 0, data: Vec::new(), container: None};
    let root: Rc<RefCell<Data>> = Rc::new(RefCell::new(storage));
    let mut current: Rc<RefCell<Data>> = Rc::clone(&root);

    for i in lines.iter() {
        if &i[0] == "$" {
            let value: &str = &i[1];
            match value {
                "cd" => {
                    if &i[2] == "/" {
                        current = Rc::clone(&root);
                    }
                    else if &i[2] == ".." {
                        if let Folder { container, .. } = &*(*Rc::clone(&current)).borrow() {
                            if let Some(point) = container {
                                current = Rc::clone(point);
                            }
                        }
                    }
                    else {
                        if let Folder { data, .. } = &mut *(*Rc::clone(&current)).borrow_mut() {
                            let mut found = false;
                            for j in (&data).iter() {
                                if let Folder { name, .. } = &*j.borrow() {
                                    if name == &i[2] {
                                        current = Rc::clone(j);
                                        found = true;
                                    }
                                }
                            }
                            if found == false {
                                println!("False find");
                                data.push(Rc::new(RefCell::new(Folder { name: i[2].clone(), size: 0, data: Vec::new(), container: Some(current.clone()) })));
                                folders.push(data[data.len() - 1].clone());
                                current = data[data.len() - 1].clone();
                            }
                        }
                    }
                },
                "ls" => {},
                _ => panic!()
            }
        }
        else if &i[0] == "dir" {
            if let Folder { data, .. } = &mut *(*Rc::clone(&current)).borrow_mut() {
                data.push(Rc::new(RefCell::new(Folder { name: i[1].clone(), size: 0, data: Vec::new(), container: Some(current.clone()) })));
                folders.push(data[data.len() - 1].clone());
            }
        }
        else {
            if let Folder { data, .. } = &mut *(*Rc::clone(&current)).borrow_mut() {
                data.push(Rc::new(RefCell::new(Data::File { name: i[1].clone(), size: i[0].parse().unwrap(), container: Some(current.clone()) })));
                folders.push(data[data.len() - 1].clone());
            }
            let mut parent: Rc<RefCell<Data>> = current.clone();
            loop {
                if let Folder { size, .. } = &mut *(*Rc::clone(&parent)).borrow_mut() {
                    *size += usize::from_str_radix(&i[0], 10).unwrap();
                }
                if let Folder { container, .. } = &*(*Rc::clone(&parent)).borrow_mut() {
                    if let Some(point) = container {
                        parent = point.clone();
                    }
                    else {
                        break;
                    }
                }
                else {
                    panic!()
                }
            }
        }
    }

    let mut size: Vec<usize> = folders.iter()
        .map(|s| {
            if let Folder { size, .. } = *s.borrow() {
                size
            }
            else {
                0
            }
        })
        .collect();

    let p1: usize = size.iter()
        .filter(|s| {
            **s <= 100000
        })
        .sum();

    let mut total: usize = 0;
    if let Folder { size, .. } = *root.borrow() {
        total = size;
    }
    let needed = 30000000 - (70000000 - total);

    let mut possible: Vec<&usize> = size.iter()
        .filter(|s| {
            **s >= needed
        })
        .collect::<Vec<&usize>>();
    possible.sort();
    
    let p2: usize = *possible[0];
    size.sort();
    println!("{:?}", size);
    println!("{}, {}", total, needed);

    println!("{}, {}", p1, p2);
    if let Folder { name, .. } = &*current.borrow() {
        println!("{}", name);
    }
    println!("{}", folders.len());
    Ok(())
}
